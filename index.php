<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">

		<title>Classes, Objects, Inheritance and Polymorphism</title>
	</head>
	<body>

		<h1>Person</h1>
		<?php echo $person->printName(); ?>

		<h1>Developer</h1>
		<?php echo $developer->printName(); ?>

		<h1>Engineer</h1>
		<?php echo $engineer->printName(); ?>

	</body>
</html>